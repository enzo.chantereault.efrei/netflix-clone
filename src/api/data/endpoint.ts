import { MovieOrYoutubeMovie } from "../../shared/models/models.index";
import { realtAcademy, realtIntros } from "./realt-youtube";

// A mock to mimic making an async request for data


export const getRealtAcademyMovies = () => {
    return new Promise<MovieOrYoutubeMovie[]>((resolve) =>
      setTimeout(() => resolve(realtAcademy), 500)
    );
}

export const getRealtAcademyMeetupMovies = () => {
  return new Promise<MovieOrYoutubeMovie[]>((resolve) =>
    setTimeout(() => resolve(realtIntros), 500)
  );
}