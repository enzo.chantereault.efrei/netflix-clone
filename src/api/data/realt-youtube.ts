import { YoutubeMovie } from "../../shared/models/models.index";

export const realtIntros: YoutubeMovie[] = [
    {
        name: "Meetup avec la communauté à Paris",
        original_name: "RealT - Meetup avec la communauté à Paris",
        overview: "RealT est la plateforme pour investir dans l'immobilier tokenisé !",
        poster_path: "RealT_Logo.png",
        backdrop_path: "RealT_Logo.svg",
        video: "Realt_meetup.mp4",
        youtubeUrl: "https://www.youtube.com/watch?v=nCjFSKxTCs4",
        isLocalData: true
    }
];

export const realtAcademy: YoutubeMovie[] = [
    {
        name: "Create a RealT account with the Walletless",
        original_name: "How to create a RealT account and buy your first RealToken with the 'walletless'",
        overview: "RealT is a platform for investing in tokenized real estate!",
        poster_path: "RealT_Logo.png",
        backdrop_path: "RealT_Logo.svg",
        video: "walletless.mp4",
        youtubeUrl: "https://www.youtube.com/watch?v=ebOyz9RJoHE&list=PLy61U4QoARx6xQ70ytImPJI2ngYA1qc9e&index=2",
        isLocalData: true
    },
    {
        name: "Buy RealTokens without fees",
        original_name: "How to buy a RealToken without credit card and without fees",
        overview: "RealT is a platform for investing in tokenized real estate!",
        poster_path: "RealT_Logo.png",
        backdrop_path: "RealT_Logo.svg",
        video: "no_fees.mp4",
        youtubeUrl: "https://www.youtube.com/watch?v=mj_ix72Y6uw&list=PLy61U4QoARx6xQ70ytImPJI2ngYA1qc9e&index=3",
        isLocalData: true
    },
    {
        name: "Acheter des RealTokens sans frais",
        original_name: "Comment acheter un RealToken sans carte de crédit et sans frais",
        overview: "RealT est la plateforme pour investir dans l'immobilier tokenisé !",
        poster_path: "RealT_Logo.png",
        backdrop_path: "RealT_Logo.svg",
        video: "no_fees.mp4",
        youtubeUrl: "https://www.youtube.com/watch?v=G9WWJcZ4Sag&list=PLy61U4QoARx6xQ70ytImPJI2ngYA1qc9e&index=4",
        isLocalData: true
    },
    {
        name: "How to use the RMM",
        original_name: "How to use the RMM",
        overview: "RealT is a platform for investing in tokenized real estate!",
        poster_path: "RealT_Logo.png",
        backdrop_path: "RealT_Logo.svg",
        video: "RMM.mp4",
        youtubeUrl: "https://www.youtube.com/watch?v=CX7YPKx958s&list=PLy61U4QoARx6xQ70ytImPJI2ngYA1qc9e&index=6",
        isLocalData: true
    },
    {
        name: "Create a RealT account with the Walletless",
        original_name: "How to create a RealT account and buy your first RealToken with the 'walletless'",
        overview: "RealT is a platform for investing in tokenized real estate!",
        poster_path: "RealT_Logo.png",
        backdrop_path: "RealT_Logo.svg",
        video: "walletless.mp4",
        youtubeUrl: "https://www.youtube.com/watch?v=ebOyz9RJoHE&list=PLy61U4QoARx6xQ70ytImPJI2ngYA1qc9e&index=2",
        isLocalData: true
    },
    {
        name: "Buy RealTokens without fees",
        original_name: "How to buy a RealToken without credit card and without fees",
        overview: "RealT is a platform for investing in tokenized real estate!",
        poster_path: "RealT_Logo.png",
        backdrop_path: "RealT_Logo.svg",
        video: "no_fees.mp4",
        youtubeUrl: "https://www.youtube.com/watch?v=mj_ix72Y6uw&list=PLy61U4QoARx6xQ70ytImPJI2ngYA1qc9e&index=3",
        isLocalData: true
    },
    {
        name: "Acheter des RealTokens sans frais",
        original_name: "Comment acheter un RealToken sans carte de crédit et sans frais",
        overview: "RealT est la plateforme pour investir dans l'immobilier tokenisé !",
        poster_path: "RealT_Logo.png",
        backdrop_path: "RealT_Logo.svg",
        video: "no_fees.mp4",
        youtubeUrl: "https://www.youtube.com/watch?v=G9WWJcZ4Sag&list=PLy61U4QoARx6xQ70ytImPJI2ngYA1qc9e&index=4",
        isLocalData: true
    },
    {
        name: "How to use the RMM",
        original_name: "How to use the RMM",
        overview: "RealT is a platform for investing in tokenized real estate!",
        poster_path: "RealT_Logo.png",
        backdrop_path: "RealT_Logo.svg",
        video: "RMM.mp4",
        youtubeUrl: "https://www.youtube.com/watch?v=CX7YPKx958s&list=PLy61U4QoARx6xQ70ytImPJI2ngYA1qc9e&index=6",
        isLocalData: true
    }
];