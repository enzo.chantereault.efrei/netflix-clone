import { environment } from "../../environments/environement";

const TMDB_API_KEY: string = environment.tmdbApiKey;

export const IMAGE_BASE_URL: string = "http://image.tmdb.org/t/p/original/";

export interface TMDBEndpoints {
    fetchTvTopRated: string;
    fetchTrending: string,
    fetchNetflixOriginals: string,
    fetchMovieTopRated: string,
    fetchActionMovies: string,
    fetchComedyMovies: string,
    fetchHorrorMovies: string,
    fetchRomanceMovies: string,
    fetchDocumentaries: string
}

const endpoints: TMDBEndpoints = {
    fetchTvTopRated: `tv/top_rated?api_key=${TMDB_API_KEY}&language=en-US`,
    fetchTrending: `trending/all/week?api_key=${TMDB_API_KEY}&language=en-US`,
    fetchNetflixOriginals: `discover/tv?api_key=${TMDB_API_KEY}&with_networks=213`,
    fetchMovieTopRated: `movie/top_rated?api_key=${TMDB_API_KEY}&language=en-US`,
    fetchActionMovies: `discover/movie?api_key=${TMDB_API_KEY}&with_genres=28`,
    fetchComedyMovies: `discover/movie?api_key=${TMDB_API_KEY}&with_genres=35`,
    fetchHorrorMovies: `discover/movie?api_key=${TMDB_API_KEY}&with_genres=27`,
    fetchRomanceMovies: `discover/movie?api_key=${TMDB_API_KEY}&with_genres=10749`,
    fetchDocumentaries: `discover/movie?api_key=${TMDB_API_KEY}&with_genres=99`
};

export default endpoints;