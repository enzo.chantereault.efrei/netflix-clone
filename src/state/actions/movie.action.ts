import { MovieOrYoutubeMovie } from "../../shared/models/models.index";
import { ActionType } from "./action-type.enum";

interface SelectMovie {
    type: ActionType.SELECT_MOVIE,
    payload: MovieOrYoutubeMovie
}

interface UnselectMovie {
    type: ActionType.UNSELECT_MOVIE
}

export type MovieAction = SelectMovie | UnselectMovie;