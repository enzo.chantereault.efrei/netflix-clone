import { ActionType } from "./action-type.enum";

interface PreviewModalOpen {
    type: ActionType.PREVIEW_MODAL_OPEN,
    payload: boolean
}

interface PreviewModalClose {
    type: ActionType.PREVIEW_MODAL_CLOSE,
    payload: boolean
}

export type PreviewModalAction = PreviewModalOpen | PreviewModalClose;