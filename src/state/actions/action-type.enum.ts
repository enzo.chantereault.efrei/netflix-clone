export enum ActionType {
    SELECT_BANNER = 0,
    SELECT_MOVIE = 1,
    UNSELECT_MOVIE = 2,
    PREVIEW_MODAL_OPEN = 3,
    PREVIEW_MODAL_CLOSE = 4
}
