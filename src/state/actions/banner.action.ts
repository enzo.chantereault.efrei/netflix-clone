import { MovieOrYoutubeMovie } from "../../shared/models/models.index";
import { ActionType } from "./action-type.enum";

interface SelectBanner {
    type: ActionType.SELECT_BANNER,
    payload: MovieOrYoutubeMovie
}

export type BannerAction = SelectBanner