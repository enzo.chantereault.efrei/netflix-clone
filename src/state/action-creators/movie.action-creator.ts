import { Dispatch } from "redux";
import { MovieOrYoutubeMovie } from "../../shared/models/models.index";
import { ActionType, MovieAction } from "../actions/actions.index";

export const selectMovie = (movie: MovieOrYoutubeMovie) => {
    return (dispatch: Dispatch<MovieAction>) => {
        dispatch({
            type: ActionType.SELECT_MOVIE,
            payload: movie
        });
    };
};

export const unselectMovie = () => {
    return (dispatch: Dispatch<MovieAction>) => {
        dispatch({
            type: ActionType.UNSELECT_MOVIE
        });
    };
};