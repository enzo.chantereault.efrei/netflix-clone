import { selectBanner } from './banner.action-creator';
import { selectMovie, unselectMovie } from './movie.action-creator';
import { openPreviewModal, closePreviewModal } from './preview-modal.action-creator';

export const actionCreators = { selectBanner, selectMovie, unselectMovie, openPreviewModal, closePreviewModal };