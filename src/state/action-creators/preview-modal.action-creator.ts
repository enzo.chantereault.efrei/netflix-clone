import { Dispatch } from "redux";
import { ActionType, PreviewModalAction } from "../actions/actions.index";

export const openPreviewModal = () => {
    return (dispatch: Dispatch<PreviewModalAction>) => {
        dispatch({
            type: ActionType.PREVIEW_MODAL_OPEN,
            payload: true
        });
    };
};

export const closePreviewModal = () => {
    return (dispatch: Dispatch<PreviewModalAction>) => {
        dispatch({
            type: ActionType.PREVIEW_MODAL_CLOSE,
            payload: false
        });
    };
};