import { Dispatch } from "redux";
import { MovieOrYoutubeMovie } from "../../shared/models/models.index";
import { ActionType, BannerAction } from "../actions/actions.index";

export const selectBanner = (movie: MovieOrYoutubeMovie) => {
    return (dispatch: Dispatch<BannerAction>) => {
        dispatch({
            type: ActionType.SELECT_BANNER,
            payload: movie
        });
    };
};