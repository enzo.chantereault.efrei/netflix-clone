import { ActionType, PreviewModalAction } from "../actions/actions.index";


const initialState: boolean = false;

export const reducer = (state = initialState, action: PreviewModalAction) => {
    let result: boolean = state;
    switch (action.type) {
        case ActionType.PREVIEW_MODAL_OPEN:
        case ActionType.PREVIEW_MODAL_CLOSE:
            result = action.payload;
            break;
        default:
            break;
    }
    return result;
};