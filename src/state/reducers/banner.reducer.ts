import { MovieOrYoutubeMovie } from "../../shared/models/models.index";
import { ActionType, BannerAction } from "../actions/actions.index";


const initialState = null;

export const reducer = (
    state: MovieOrYoutubeMovie = initialState, 
    action: BannerAction) => {
    let result = state;
    switch (action.type) {
        case ActionType.SELECT_BANNER:
            result = action.payload;
            break;
        default:
            break;
    }
    return result;
};