import { combineReducers } from "redux";
import { reducer as bannerReducer} from "./banner.reducer";
import { reducer as movieReducer } from "./movie.reducer";
import { reducer as previewModelReducer} from "./preview-modal.reducer";

export const reducers = combineReducers({
    banner: bannerReducer,
    movie: movieReducer,
    previewModal: previewModelReducer
});

export type State = ReturnType<typeof reducers>;        