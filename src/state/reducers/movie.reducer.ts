import { MovieOrYoutubeMovie } from "../../shared/models/models.index";
import { ActionType, MovieAction } from "../actions/actions.index";


const initialState = null;

export const reducer = (
    state: MovieOrYoutubeMovie = initialState, 
    action: MovieAction) => {
    let result = state;
    switch (action.type) {
        case ActionType.SELECT_MOVIE:
            result = action.payload;
            break;
        case ActionType.UNSELECT_MOVIE:
            result = null;
            break;
        default:
            break;
    }
    return result;
};