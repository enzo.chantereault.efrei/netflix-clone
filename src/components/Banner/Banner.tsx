import { useEffect } from 'react';
import ReactPlayer, { Config } from 'react-player';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getRealtAcademyMeetupMovies } from '../../api/data/endpoint';
import { IMAGE_BASE_URL } from '../../api/tmdb/tmdb-endpoints';
import { MovieOrYoutubeMovie } from '../../shared/models/models.index';
import { isYoutubeMovie } from '../../shared/utils';
import { actionCreators, State } from '../../state';
import Loader from '../Loader/Loader';
import './Banner.scss';

export interface BannerProps {
    title?: string;
    description?: string;
    isTrailerOn?: boolean;
}

const Banner = ({title = "", description = "", isTrailerOn = true}: BannerProps) => {
    const state = useSelector((state: State) => state);
    const dispatch = useDispatch();
    const {selectBanner, selectMovie, openPreviewModal} = bindActionCreators(actionCreators, dispatch);

    useEffect(
        () => {
            getRealtAcademyMeetupMovies().then(
                (response: MovieOrYoutubeMovie[]) => {
                    selectBanner(response[Math.floor(Math.random() * response.length)]);
                }
            );
        },
        []
    );

    const openModal = () => {
        selectMovie(state.banner);
        openPreviewModal();
    }

    const truncate = (text: string, size: number) => {
        return  text?.length > size ? text.substring(0, size - 1) + "..." : text ;
    }

    const openNewTab = (url?: string) => {
        if(!url) {
            return;
        }
        window.open(url, "_blank");
    }

    const videoConfig: Config = {
        file: { 
            attributes: { 
                poster: `${IMAGE_BASE_URL}${state.banner?.backdrop_path}`
            } 
        }
    };

    const internalUrl = {src: isYoutubeMovie(state.banner) ? `${process.env.PUBLIC_URL}/videos/${state.banner?.video}` : '', type: 'video/mp4'};

    const player: JSX.Element = <ReactPlayer className='banner-player' url={[internalUrl]} volume={0.65} width="100%" height="100%" playsinline playing={!state.previewModal && isTrailerOn} muted loop config={videoConfig}/>

    const poster = <div className='banner-player' style={{width: '100%', height: '100%', backgroundPosition: 'center', backgroundSize: 'cover', backgroundImage: `url(${!state.banner?.isLocalData ? IMAGE_BASE_URL : process.env.PUBLIC_URL + '/items/'}${state.banner?.backdrop_path})`}}></div>

    const loader = <div className='banner-player' style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}><Loader /></div>

    return (
        <header>
            { !state.banner ? loader : isYoutubeMovie(state.banner) ? player : poster }
            <div className='banner-container'>
                <h1 className='banner-title'>{state.banner?.name ?? state.banner?.original_name ?? title}</h1>
                <h2 className='banner-description'>
                    {truncate(state.banner?.overview ?? description, 150)}
                </h2>
                <div className='banner-button-container'>
                    <button 
                        className='banner-button primary-button'
                        onClick={() => isYoutubeMovie(state.banner) ? openNewTab(state.banner.youtubeUrl) : null}>
                            <svg 
                                width="24" 
                                height="24" 
                                viewBox="0 0 24 24" 
                                fill="none" 
                                xmlns="http://www.w3.org/2000/svg" 
                                className="Hawkins-Icon Hawkins-Icon-Standard">
                                <path d="M4 2.69127C4 1.93067 4.81547 1.44851 5.48192 1.81506L22.4069 11.1238C23.0977 11.5037 23.0977 12.4963 22.4069 12.8762L5.48192 22.1849C4.81546 22.5515 4 22.0693 4 21.3087V2.69127Z" fill="currentColor"></path>
                            </svg>
                            &nbsp;
                            <span className='button-text'>Play</span>
                    </button>
                    <button 
                        className='banner-button secondary-button'
                        onClick={() => openModal()}>
                        <svg 
                            width="24" 
                            height="24" 
                            viewBox="0 0 24 24" 
                            fill="none" 
                            xmlns="http://www.w3.org/2000/svg" 
                            className="Hawkins-Icon Hawkins-Icon-Standard">
                                <path fillRule="evenodd" clipRule="evenodd" d="M12 3C7.02944 3 3 7.02944 3 12C3 16.9706 7.02944 21 12 21C16.9706 21 21 16.9706 21 12C21 7.02944 16.9706 3 12 3ZM1 12C1 5.92487 5.92487 1 12 1C18.0751 1 23 5.92487 23 12C23 18.0751 18.0751 23 12 23C5.92487 23 1 18.0751 1 12ZM13 10V18H11V10H13ZM12 8.5C12.8284 8.5 13.5 7.82843 13.5 7C13.5 6.17157 12.8284 5.5 12 5.5C11.1716 5.5 10.5 6.17157 10.5 7C10.5 7.82843 11.1716 8.5 12 8.5Z" fill="currentColor"></path>
                        </svg>
                        &nbsp;
                        <span className='button-text'>More info</span>
                    </button>
                </div>
            </div>
            <div className='banner-fade-bottom'></div>
        </header>
    )
};

export default Banner;