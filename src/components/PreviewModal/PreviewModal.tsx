import { Dispatch, SetStateAction, useEffect, useState } from "react";
import ReactPlayer from "react-player";
import { IMAGE_BASE_URL } from "../../api/tmdb/tmdb-endpoints";
import { MovieOrYoutubeMovie } from "../../shared/models/models.index";
import { isYoutubeMovie } from "../../shared/utils";
import "./PreviewModal.scss";

export interface PreviewModalProps {
    title?: string;
    description?: string;
    isOpen?: boolean;
    movie: MovieOrYoutubeMovie;
    closeModal: () => void;
}

const PreviewModal = ({
    title = "", 
    description = "",
    isOpen = false,
    movie,
    closeModal
}: PreviewModalProps) => {

    const [movieTeaserUrl, setMovieTeaserUrl]: [string, Dispatch<SetStateAction<string>>] = useState(''); 

    useEffect(
        () => {
            let teaserUrl: string = '';
            if(movie?.isLocalData) {
                teaserUrl = isYoutubeMovie(movie) ? movie.youtubeUrl as string : '';
            }
            setMovieTeaserUrl(teaserUrl);
        },
        [movie]
    );

    const player: JSX.Element = <ReactPlayer className='preview-modal-banner' url={movieTeaserUrl} volume={0.65} width="100%" playsinline playing={isOpen} />

    const poster = <div className='preview-modal-banner' style={{backgroundSize: 'cover', backgroundPosition: 'center', backgroundImage: `url(${!movie?.isLocalData ? IMAGE_BASE_URL : process.env.PUBLIC_URL + '/items/'}${movie?.backdrop_path})`}}></div>

    if(!isOpen) {
        return null;
    }
    return (
        <div className='preview-modal-container'>
            { isYoutubeMovie(movie) ? player : poster }
            <div className="preview-modal-content">
                    <h3 className="preview-modal-title">{movie?.name ?? movie?.original_name ?? title}</h3>
                    <p>{movie?.overview ?? description}</p>
                </div>
                <div 
                    className="preview-modal-close"
                    onClick={closeModal}>
                    <svg 
                        width="24" 
                        height="24" 
                        viewBox="0 0 24 24" 
                        fill="none" 
                        xmlns="http://www.w3.org/2000/svg" 
                        className="close-icon" 
                        data-uia="previewModal-closebtn" 
                        role="button" 
                        aria-label="close">
                        <path fillRule="evenodd" clipRule="evenodd" d="M2.29297 3.70706L10.5859 12L2.29297 20.2928L3.70718 21.7071L12.0001 13.4142L20.293 21.7071L21.7072 20.2928L13.4143 12L21.7072 3.70706L20.293 2.29285L12.0001 10.5857L3.70718 2.29285L2.29297 3.70706Z" fill="currentColor"></path>
                    </svg>
                </div>
        </div>
    )
};

export default PreviewModal;