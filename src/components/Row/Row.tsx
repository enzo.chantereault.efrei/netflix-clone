import { Dispatch, SetStateAction, useEffect, useState } from 'react';
import './Row.scss';
import instance from '../../api/tmdb/tmdb-axios';
import { AxiosResponse } from 'axios';
import { MovieOrYoutubeMovie } from '../../shared/models/models.index';
import PostersTop10 from './PostersTop10/PostersTop10';
import { IMAGE_BASE_URL } from '../../api/tmdb/tmdb-endpoints';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from '../../state';
import { getRealtAcademyMovies } from '../../api/data/endpoint';

export interface RowProps {
  title?: string;
  fetchUrl?: string;
  isTop10Display?: boolean;
}

const Row = ({title = "", fetchUrl, isTop10Display = false}: RowProps) => {
  const dispatch = useDispatch();
  const {selectMovie, openPreviewModal} = bindActionCreators(actionCreators, dispatch);
  const [movies, setMovies]: [MovieOrYoutubeMovie[], Dispatch<SetStateAction<MovieOrYoutubeMovie[]>>] = useState([] as MovieOrYoutubeMovie[]);

  useEffect(
    () => {
      if(!fetchUrl) {
        getRealtAcademyMovies().then(
          (response: MovieOrYoutubeMovie[]) => {
            setMovies(response);
          }
        );
      } else {
        instance.get(fetchUrl!)
        .then((response: AxiosResponse<{page: number, results: MovieOrYoutubeMovie[], total_pages: number, total_results: number}, any>) => {
          setMovies(response.data.results);
        });
      }
    },
    [fetchUrl]
  );

  const openModal = (movie: MovieOrYoutubeMovie) => {
    selectMovie(movie);
    openPreviewModal();
  }

  const posters: JSX.Element[] = movies.map((movie: MovieOrYoutubeMovie, index: number) => 
    <img key={movie?.id ?? index} className={`large-poster${!movie?.isLocalData ? '' : ' bg-white'}`} src={`${!movie?.isLocalData ? IMAGE_BASE_URL : process.env.PUBLIC_URL + '/items/'}${movie?.backdrop_path}`} alt={movie?.name ?? movie?.original_name} onClick={() => openModal(movie)}></img>
  );

  return (
    <div className='row-container'>
      <h2 className='row-header'>
        <div className='row-header-title'>{title}</div>
      </h2>
        {!isTop10Display ? <div className='row-posters'>{posters}</div> : <PostersTop10 movies={movies} openModal={openModal}/>}
    </div>
  )
}

export default Row;