import './Navbar.scss';

export interface NavbarProps {
    isBgTransparent: boolean;
}

const Navbar = ({isBgTransparent }: NavbarProps) => {

    return (
        <div className={`navbar${isBgTransparent ? " show" : ''}`}>
            <div className="navbar-content">
                <img
                    className='navbar-logo'
                    src="https://upload.wikimedia.org/wikipedia/commons/0/08/Netflix_2015_logo.svg"
                    alt="netflix-logo">
                </img>
                <img
                    className='navbar-avatar'
                    src="https://upload.wikimedia.org/wikipedia/commons/0/0b/Netflix-avatar.png"
                    alt="netflix-avatar">
                </img>
            </div>
        </div>
    )
};

export default Navbar;