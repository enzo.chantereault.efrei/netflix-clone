export class Movie {
    id?: number | null;
    name!: string;
    original_name!: string;
    overview!: string;
    poster_path!: string;
    backdrop_path!: string;
    original_language?: string;
    origin_country?: string[]; 
    genre_ids?: number[]; 
    popularity?: number;
    vote_average?: number;
    vote_count?: number;
    first_air_date?: string;
    isLocalData: boolean = false;
    video?: string;
    youtubeUrl?: string;
}

export class YoutubeMovie extends Movie {

    constructor(video?: string) {
        super();
        this.video = video;
    }
}

export type MovieOrYoutubeMovie = Movie | YoutubeMovie | null;