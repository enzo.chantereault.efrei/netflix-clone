import { MovieOrYoutubeMovie, YoutubeMovie } from "./models/models.index"

export const isYoutubeMovie = (toBeDetermined: MovieOrYoutubeMovie): toBeDetermined is YoutubeMovie => {
    if((toBeDetermined as YoutubeMovie)?.video){
      return true
    }
    return false
  }