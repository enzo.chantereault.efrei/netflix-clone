import { Dispatch, SetStateAction, useEffect, useState } from 'react';
import './App.scss';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators, State } from './state';
import endpoints from './api/tmdb/tmdb-endpoints'
import Row from './components/Row/Row';
import Banner from './components/Banner/Banner';
import Navbar from './components/Navbar/Navbar';
import PreviewModal from './components/PreviewModal/PreviewModal';

const App = () => {
  const state = useSelector((state: State) => state);
  const dispatch = useDispatch();
  const {closePreviewModal} = bindActionCreators(actionCreators, dispatch);
  const [isNavBgTransparent, setIsNavBgTransparent]: [boolean, Dispatch<SetStateAction<boolean>>] = useState(false);

  const handleShow = (event: Event) => {
    if(window.scrollY > 100) {
      setIsNavBgTransparent(true);
    } else {
      setIsNavBgTransparent(false);
    }
  }

  useEffect(
      () => {
        window.addEventListener("scroll", handleShow);
        return () => window.removeEventListener("scroll", handleShow);  
      },
      []
  );
  
  return (
    <div className='app'>
      <PreviewModal isOpen={state.previewModal} movie={state.movie} closeModal={closePreviewModal}/>
      <Navbar isBgTransparent={isNavBgTransparent}/>
      <Banner />
      <Row 
        title='RealT Academy'
        isTop10Display={false}/>
      <Row 
        title='Top 10 TV Shows'
        fetchUrl={endpoints.fetchTvTopRated}
        isTop10Display={true}/>
      <Row 
        title='Trending Now'
        fetchUrl={endpoints.fetchTrending}/>
      <Row 
        title='Netflix Originals'
        fetchUrl={endpoints.fetchNetflixOriginals}/>
      <Row 
        title='Top 10 Movies'
        fetchUrl={endpoints.fetchMovieTopRated}
        isTop10Display={true}/>
      <Row 
        title='Actions'
        fetchUrl={endpoints.fetchActionMovies}/>
      <Row 
        title='Comedies'
        fetchUrl={endpoints.fetchComedyMovies}/>
      <Row 
        title='Horrors'
        fetchUrl={endpoints.fetchHorrorMovies}/>
      <Row 
        title='Romances'
        fetchUrl={endpoints.fetchRomanceMovies}/>
      <Row 
        title='Documentaries'
        fetchUrl={endpoints.fetchDocumentaries}/>
    </div>
  );
};

export default App;